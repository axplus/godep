package main

import (
	"flag"
	"fmt"
	"go/build"
	"strings"
)

type Digraph struct {
	nodes map[string]*Node
}

type Node struct {
	id    int
	label string
	refs  []string
	beref bool
}

func (dig *Digraph) compute() {
	i := 0
	backrefmap := make(map[string]bool)
	for _, n := range dig.nodes {
		n.id = i
		i++
		for _, ref := range n.refs {
			backrefmap[ref] = true
		}
	}
	for pkg, _ := range backrefmap {
		if n, ok := dig.nodes[pkg]; ok {
			n.beref = true
		}
	}
}

func (dig Digraph) PrintAll() {
	fmt.Printf("digraph a {\n")
	// fmt.Printf("rankdir=\"LR\"")
	for _, n := range dig.nodes {
		label := strings.Replace(n.label, "git.garena.com/shopee-server/crawler/", "", -1)
		attr := make([]string, 0)
		attr = append(attr,
			fmt.Sprintf("label=\"%s\"", label),
		)
		if !n.beref {
			attr = append(attr, "color=red")
		}

		fmt.Printf("_%d [%s];\n", n.id, strings.Join(attr, ","))
		for _, r := range n.refs {
			if refn, ok := dig.nodes[r]; ok {
				fmt.Printf("_%d -> _%d;\n", n.id, refn.id)
			}
		}
	}
	fmt.Printf("}\n")
}
func NewDigraph() *Digraph {
	d := &Digraph{
		nodes: make(map[string]*Node),
	}
	return d
}

var (
	buildContext = build.Default
)

func main() {
	flag.Parse()

	dig := NewDigraph()

	for _, pkgName := range flag.Args() {
		_, err := gatherimports(pkgName, dig)
		if err != nil {
			panic(err)
		}
	}
	dig.compute()
	dig.PrintAll()
}

var i = 0

func gatherimports(pkgname string, dig *Digraph) (alreadyimported bool, err error) {

	if _, ok := dig.nodes[pkgname]; ok {
		alreadyimported = true
		return
	}

	pkg, err := buildContext.Import(pkgname, ".", 0)
	if err != nil {
		return
	}
	node := &Node{
		id:    i,
		label: pkg.ImportPath,
		beref: false,
	}
	i++
	if pkg.Goroot {
		alreadyimported = true
		return
	}

	// pkgs[pkgname] = pkg
	dig.nodes[pkgname] = node

	// fmt.Println(pkg.ImportPath, pkg.Goroot)

	for _, imp := range pkg.Imports {
		// pkgs[imp] = nil
		if imp == pkg.ImportPath {
			continue
		}

		node.refs = append(node.refs, imp)
		// fmt.Println(imp)
		gatherimports(imp, dig)
	}
	return
}
